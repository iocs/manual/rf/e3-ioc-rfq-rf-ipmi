# e3-ioc-rfq-rf-ipmi

IOC repository for RF station microTCA monitoring.

**IOC-name**: `RFQ-010:SC-IOC-107`

```
FRU Information:

 FRU  Device   State  Name
  0   MCH       M4    NAT-MCH-CM
  3   mcmc1     M4    NAT-MCH-MCMC
  5   AMC1      M4    CCT AM G64/471
  6   AMC2      M4    mTCA-EVR-300
  7   AMC3      M4    SIS8300KU AMC
  8   AMC4      M4    SIS8300KU AMC
  9   AMC5      M4    SIS8300KU AMC
 10   AMC6      M4    SIS8300KU AMC
 13   AMC9      M4    IOxOS IFC-1410
 40   CU1       M4    Schroff uTCA CU
 41   CU2       M4    Schroff uTCA CU
 51   PM2       M4    PM-AC1000
 53   PM4       M4    PM-AC1000
 60   Clock1    M4    MCH-Clock
 61   HubMod1   M4    MCH-PCIe
 92   AMC3-RTM  M4    SIS8300KU RTM
 93   AMC4-RTM  M4    SIS8300KU RTM
 94   AMC5-RTM  M4    SIS8300KU RTM
 95   AMC6-RTM  M4    SIS8300KU RTM
 98   AMC9-RTM  M4    RTM
```

