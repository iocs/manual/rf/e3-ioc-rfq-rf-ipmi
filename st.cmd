require ipmimanager
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")

# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 1)

epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "rfq-rf-mtca-mch.tn.esss.lu.se")
epicsEnvSet("TELNET_PORT", "23")

# INTERNAL archiver
# 1=enable/0=disable
epicsEnvSet("ARCHIVER", "0")
epicsEnvSet("ARCHIVER_SIZE", 1024)

# Panels compatibility version
epicsEnvSet("PANEL_VER", "2.0.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode
epicsEnvSet("NAME_MODE", 0)

epicsEnvSet("DEPLOYMENT_DIR", "auto-gen-db")

epicsEnvSet("P", "RFQ-010:RFS")
epicsEnvSet("MTCA_PREF", "$(P)-MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF", "$(P)-IOC-$(CRATE_NUM)00:")

epicsEnvSet("SLOT1_MODULE", "CPU")
epicsEnvSet("SLOT1_IDX", "01")
epicsEnvSet("SLOT2_MODULE", "EVR")
epicsEnvSet("SLOT2_IDX", "01")
epicsEnvSet("SLOT3_MODULE", "DIG")
epicsEnvSet("SLOT3_IDX", "01")
epicsEnvSet("SLOT4_MODULE", "DIG")
epicsEnvSet("SLOT4_IDX", "02")
epicsEnvSet("SLOT5_MODULE", "DIG")
epicsEnvSet("SLOT5_IDX", "03")
epicsEnvSet("SLOT6_MODULE", "DIG")
epicsEnvSet("SLOT6_IDX", "04")
epicsEnvSet("SLOT9_MODULE", "AMC")
epicsEnvSet("SLOT9_IDX", "01")
epicsEnvSet("SLOT19_MODULE", "RTM")
epicsEnvSet("SLOT19_IDX", "01")
epicsEnvSet("SLOT20_MODULE", "RTM")
epicsEnvSet("SLOT20_IDX", "02")
epicsEnvSet("SLOT21_MODULE", "RTM")
epicsEnvSet("SLOT21_IDX", "03")
epicsEnvSet("SLOT22_MODULE", "RTM")
epicsEnvSet("SLOT22_IDX", "04")

epicsEnvSet("CHASSIS_CONFIG","SLOT1_MODULE=$(SLOT1_MODULE)", "SLOT1_IDX=$(SLOT1_IDX)", "SLOT2_MODULE=$(SLOT2_MODULE)", "SLOT2_IDX=$(SLOT2_IDX)", "SLOT3_MODULE=$(SLOT3_MODULE)","SLOT3_IDX=$(SLOT3_IDX)", "SLOT4_MODULE=$(SLOT4_MODULE)", "SLOT4_IDX=$(SLOT4_IDX)", "SLOT5_MODULE=$(SLOT5_MODULE)", "SLOT5_IDX=$(SLOT5_IDX)", "SLOT6_MODULE=$(SLOT6_MODULE)", "SLOT6_IDX=$(SLOT6_IDX)", "SLOT9_IDX=$(SLOT9_IDX)", "SLOT9_MODULE=$(SLOT9_MODULE)", "SLOT19_IDX=$(SLOT19_IDX)", "SLOT19_MODULE=$(SLOT19_MODULE)", "SLOT20_IDX=$(SLOT20_IDX)", "SLOT20_MODULE=$(SLOT20_MODULE)", "SLOT21_IDX=$(SLOT21_IDX)", "SLOT21_MODULE=$(SLOT21_MODULE)", "SLOT22_IDX=$(SLOT22_IDX)", "SLOT22_MODULE=$(SLOT22_MODULE)") 

# load db templates for your hardware configuration
iocshLoad("chassis.iocsh")

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic, MCH_ADDR=$(MCH_ADDR), ARCHIVER=$(ARCHIVER), ARCHIVER_SIZE=$(ARCHIVER_SIZE), P=$(P), CRATE_NUM=$(CRATE_NUM), TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),CHECK_VER=#,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

iocInit()

eltc "$(DISP_MSG)"
